# CsvParser

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7.

#How to install
1. Download the code
2. run 'npm install' to install project dependencies.
3. run 'ng serve --open' to open the application in chrome


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## General information

This project has been created with keeping following things in mind:

1. Each feature will have its own module and framework has ability to lazy load those module.
2. Shared folder is created to share common functionality across the application. It has utility module which provide diffrent utilities and validation those are going to reused in the application.
3. Core folder has all core information about project like models, services those are going to use across application.
4. CSV parser component has a form which accepts csv string which is validated by utlity service and parsed with the help of utility helper.
5. CSV parser service has created which will connect to backend and return the json output in defined CSVParser model format. Common error handling functionality is setup so all the errors will be handled at one place.