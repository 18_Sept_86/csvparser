export interface CSVParser {
    borrower_name?: string;
    status?: string;
    loan_number?: number;
  }
