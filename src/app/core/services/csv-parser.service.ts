import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { CSVParser } from '../models';

@Injectable()
export class CsvParserService {

  constructor(private apiService: ApiService) {  }
  /**
   * service call to get data in key value pair format.
   * @param stringToParse
   */
  getParsedJSONData(stringToParse):  Observable<CSVParser>  {
  return this.apiService.get(`dummy/handler/parsedJSON.json`, stringToParse)
    .pipe(map(jsonStr => {
      return jsonStr.data;
    }));
  }
}
