import { Component, Input } from '@angular/core';

import { Errors } from '../../../core';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html'
})
export class ErrorMessageComponent {
  formattedErrors: Array<string> = [];

  @Input()
  set errors(errorMessages: Errors) {
    this.formattedErrors = Object.keys(errorMessages.errors || {})
      .map(key => `${key} ${errorMessages.errors[key]}`);
  }

  get errorMessages() { return this.formattedErrors; }


}
