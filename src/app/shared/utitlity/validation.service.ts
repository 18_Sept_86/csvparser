import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { checkStringFormat } from '../utitlity/utility.helpers';

@Injectable()
export class ValidationService {

  /**
   * Validates required string value for csv parser
   *
   * @param formControl
   */
  public validateFormat(formControl: FormControl): {[error: string]: any} {
    const csvStr = formControl.value;
    /**
     *  Validate format
     * 1. String with ending colon
     * 2. String has colon and equal sign in sequence
     */
    if (checkStringFormat(csvStr, [/\:$/, /\:\=/])) {
      return {validateFormat : {valid : true} };
    }
    return null;
  }
}
