/**
 * Returns true if the given string is blank, null or undefined
 *
 * @param str
 */
export function isBlank(str: string) {
  return (!str || /^\s*$/.test(str));
}


/**
 * Return array of split string with specific input character
 */
export function splitString(stringToSplit: string, char: string) {
    if (isBlank(stringToSplit)) {
        return;
    }
    return stringToSplit.split(char);
}

/**
 * Return if string can be converted to key - value format
 * @param str
 */

export function checkStringFormat(str: string, format: Array<RegExp>): any {
    let isValidString = true;
    format.forEach(regExp => {
        if (regExp.test(str) && isValidString) {
            isValidString = false;
        }
    });
    return !isValidString;
}

/**
 * Returns formatted Object in [Key - Value ] pair form
 * @param csvString
 */
export function convertCSVToJSON(csvString: string) {
    let generateKeyValuePair = [];
    const jsonString = {};

    generateKeyValuePair = this.splitString(csvString, ':').map(str => this.splitString(str, '='));

    generateKeyValuePair.forEach(([key, value]) => {
        jsonString[key] = value;
    });
    return jsonString;
}
