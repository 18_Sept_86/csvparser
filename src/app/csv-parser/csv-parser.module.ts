import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  SharedModule
} from '../shared';
import { CsvParserRoutingModule } from './csv-parser-routing.module';
import { CsvParserComponent } from './csv-parser.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CsvParserRoutingModule
  ],
  declarations: [CsvParserComponent]
})
export class CsvParserModule { }
