import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';

import { convertCSVToJSON } from '../shared';
import { CsvParserService } from '../core';
import {CSVParser} from '../core/models'

import {
  ValidationService
} from '../shared'

@Component({
  selector: 'app-csv-parser',
  templateUrl: './csv-parser.component.html',
  styleUrls: ['./csv-parser.component.css']
})
export class CsvParserComponent implements OnInit {
  public csvParserForm: FormGroup;
  public inputString:  AbstractControl;
  public parsedJSON: CSVParser;
  constructor(
    private fb: FormBuilder,
    private validationService: ValidationService,
    private csvParserService: CsvParserService
  ) {
  }

  /**
   * Initialize form controls
   */
  initFormControls() {
    this.csvParserForm = this.fb.group(
      {
        inputString: ['', [Validators.required, this.validationService.validateFormat]]
      });
      this.inputString = this.csvParserForm.controls['inputString'];
  }

  ngOnInit() {
    this.initFormControls();
  }

  onSubmit() {
    if (this.csvParserForm.invalid) {
        return;
    }
    this.parsedJSON = convertCSVToJSON(this.inputString.value);
    this.csvParserForm.reset();
  }

  /**
   * Webservice call to get the json object
   * @param csvStr
   */
  parsedCSV_Via_Service(csvStr): void {
    this.csvParserService.getParsedJSONData(csvStr)
    .subscribe(parsedString => {
      this.parsedJSON = parsedString;
    }, err => {
      console.log('Something went wrong ', err);
    });
  }

}
