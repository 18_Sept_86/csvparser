import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PageNotFoundComponent } from './shared';

const routes: Routes = [
  {path: '', redirectTo: '/csv-parser', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];
// All feature modules needs to be preloaded here.

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
